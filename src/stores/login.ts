import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessagerStore } from "./message";
export const useLoginStore = defineStore("login", () => {
  const userStore = useUserStore();
  const messageStore = useMessagerStore();
  const loginName = ref("");
  const isLogin = computed(() => {
    return loginName.value !== "";
  });
  const login = (userName: string, password: string): void => {
    if (userStore.login(userName, password)) {
      loginName.value = userName;
      localStorage.setItem("loginName", userName);
    } else {
      messageStore.showMessage("Login ไม่ถูกต้อง");
    }
  };

  const logout = () => {
    loginName.value = "";
    localStorage.removeItem("loginName");
  };
  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData };
});
